import Meta from "../meta/Meta";

export const Home = () => {
  return (
    <div className="home-page">
      <Meta />
      <div className="home">
        <div className="container">
          <h3>In this blog Project</h3>

          <ul>
            <li>
              NextJs <br />
              <span>
                Introducing Next. js · Best SEO practices · Caching and
                Automatic Static Optimization built-in · Fully server-rendered
                pages · 100% React support ...
              </span>
            </li>

            <li>
              Next.js; what is it and why do we use it? <br />
              <span>
                Next. js is clever enough to only load the Javascript and CSS
                that are needed for any given page. This makes for much faster
                page loading times, as a user's browser doesn't have to download
                Javascript and CSS that it doesn't need for the specific page
                the user is viewing.
              </span>
            </li>

            <li>Project setup</li>
            <li>App structure</li>
            <li>Create blog content</li>
            <li>use getStaticProps method</li>
            <li>use getStaticPaths method</li>
            <li>use axios</li>
            <li>Seo</li>
            <li>Performance</li>
            <li>CRUD (create,read,update,delete)</li>
          </ul>
        </div>
      </div>
    </div>
  );
};
