import Link from "next/link";
import { FaLinkedin, FaGitlab } from "react-icons/fa";
const Footer = () => {
  return (
    <footer className="footer">
      <div className="linkedin">
        <Link href="https://www.linkedin.com/in/taherzobeydi/">
          <span>
            <FaLinkedin />
          </span>
        </Link>
      </div>

      <div className="gitlab">
        <Link href="https://gitlab.com/Taaher/">
          <span>
            <FaGitlab />
          </span>
        </Link>
      </div>
    </footer>
  );
};

export default Footer;
