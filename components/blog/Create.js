import { useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import Meta from "../meta/Meta";

const Create = ({ close }) => {
  const router = useRouter();
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");

  const onSubmit = async (e) => {
    e.preventDefault();
    const post = {
      title,
      body,
    };
    const res = await axios.post("https://tzjson.herokuapp.com/posts/", post);
    console.log(res.data, "saved");
    router.push("/blog");
    close();
  };

  return (
    <div className="create">
      <Meta title="Create POST | DEMO "/>
      <div className="container">
        <div className="form">
          <form onSubmit={onSubmit}>
            <input
              className="create-input"
              type="text"
              placeholder="Title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
            <input
              className="create-input"
              type="text"
              placeholder="body"
              value={body}
              onChange={(e) => setBody(e.target.value)}
            />
            <button className="btn-post" type="submit">
              Send
            </button>
            <button className="btn-cancel" type="submit" onClick={close}>
              Cancel
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Create;
