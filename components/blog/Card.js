import Link from "next/link";

const Card = ({ id, title, body }) => {
  return (
    <Link href={"/blog/" + id}>
      <div className="main-card">
        <div className="card">
          <div className="title">
            <h6>{title}</h6>
          </div>
          <p>{body}</p>
        </div>
      </div>
    </Link>
  );
};

export default Card;
