import Head from "next/head";

const Meta = ({ title, keywords, description }) => {
  return (
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="keywords" content={keywords} />
      <meta name="description" content={description} />
      <meta charSet="utf-8" />
      <link rel="icon" href="/blueTz.svg" />
      <title>{title}</title>
    </Head>
  );
};
Meta.defaultProps = {
  title: "Taher",
  keywords: "Junior Full-stack Web Developer",
  description: "Demo Next-js and Crud with Server-json",
};

export default Meta;
