import { FaTrashAlt, FaRegEdit } from "react-icons/fa";
import axios from "axios";
import Modal from "react-modal";
import { useState } from "react";
import Update from "../../components/blog/Update";
import { useRouter } from "next/router";
import Meta from "../../components/meta/Meta";

const customStyles = {
  content: {
    backgroundColor: "black",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};
export const getStaticPaths = async () => {
  const res = await fetch("https://tzjson.herokuapp.com/posts/");
  const data = await res.json();

  const paths = data.map((post) => {
    return {
      params: { id: post.id.toString() },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

//
export const getStaticProps = async (context) => {
  const id = context.params.id;
  const res = await fetch("https://tzjson.herokuapp.com/posts/" + id);
  const data = await res.json();
  return {
    props: { post: data },
  };
};

const Detail = ({ post }) => {
  const router = useRouter();
  const [modalIsOpen, setIsOpen] = useState(false);
  if (!post) return <h2>Not post!</h2>;

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const deletePost = async (id) => {
    const res = await axios.delete(`https://tzjson.herokuapp.com/posts/${id}`);
    console.log(res.data, "deleted");
    router.push("/blog");
  };
  return (
    <div className="main-card">
      <div className="card">
        <Meta title="Detail | POST" />
        <div className="title">
          <h6>{post.title}</h6>
          <div>
            <FaTrashAlt onClick={() => deletePost(post.id)} />

            <div className="modal">
              <div>
                <FaRegEdit onClick={openModal} />
                <Modal
                  isOpen={modalIsOpen}
                  onRequestClose={closeModal}
                  style={customStyles}
                  contentLabel="Example Modal"
                  ariaHideApp={false}
                >
                  <Update close={closeModal} id={post.id} />
                </Modal>
              </div>
            </div>
          </div>
        </div>
        <p>{post.body}</p>
      </div>
    </div>
  );
};

export default Detail;
