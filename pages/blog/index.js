import Card from "../../components/blog/Card";
import Modal from "react-modal";
import { useState, useEffect } from "react";
import Create from "../../components/blog/Create";
import Meta from "../../components/meta/Meta";

const customStyles = {
  content: {
    backgroundColor: "black",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

export const getStaticProps = async () => {
  const res = await fetch("https://tzjson.herokuapp.com/posts/");
  const data = await res.json();

  return {
    props: { posts: data },
  };
};

const Blog = ({ posts }) => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  return (
    <div className="blog">
      <Meta title="BLOG | ALL POST! " />
      <div className="modal">
        <div>
          <div className="create-post">
            <button onClick={openModal} className="btn-green">
              create post
            </button>
          </div>

          <Modal
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            style={customStyles}
            ariaHideApp={false}
            contentLabel="Example Modal"
          >
            <Create close={closeModal} />
          </Modal>
        </div>
      </div>
      {posts &&
        posts.map((post) => (
          <Card
            title={post.title}
            body={post.body}
            id={post.id}
            key={post.id}
          />
        ))}
    </div>
  );
};

export default Blog;
