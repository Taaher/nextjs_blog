import Meta from "../components/meta/Meta";
import Link from "next/link";
const About = () => {
  return (
    <div className="about">
      <Meta
        title="About"
        description="About page nextjs"
        keywords="nextjs and nodejs"
      />
      <div className="about-me">
        <div>
          <h5>Developer : Taher</h5>
          <h5>
            gitlab:{" "}
            <Link href="https://gitlab.com/Taaher/nextjs_blog" passHref={true}>
              go to source code!
            </Link>{" "}
          </h5>
        </div>
      </div>
    </div>
  );
};

export default About;
